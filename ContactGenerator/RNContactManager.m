//
//  RNContactManager.m
//  ContactGenerator
//
//  Created by 레인 on 13. 4. 23..
//
//


#import "RNContactManager.h"
#import <AddressBookUI/AddressBookUI.h>


@interface RNContactManager (RNPrivate)
+ (NSArray *)firstNames;
+ (NSArray *)lastNames;
@end


@implementation RNContactManager

+ (void)generateForContactCount:(NSUInteger)aCount completion:(void (^)())aCompletionBlock
{
    ABAddressBookRef addressBook;
    bool wantToSaveChanges = YES;
    CFErrorRef error = NULL;

    addressBook = ABAddressBookCreate();

    for (NSInteger sIndex = 0 ; sIndex < aCount ; sIndex++)
    {
        ABRecordRef aRecord = ABPersonCreate();
        CFErrorRef  anError = NULL;

        if (!ABRecordSetValue(aRecord, kABPersonFirstNameProperty, (__bridge CFStringRef)[self.firstNames objectAtIndex:(arc4random() % self.firstNames.count)], &anError))
        {
            [[[UIAlertView alloc] initWithTitle:@"First name set fail." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            wantToSaveChanges = NO;
            break;
        }
//        else
//        {
//            NSString *name = (__bridge NSString *)ABRecordCopyValue(aRecord, kABPersonFirstNameProperty);
//            [[[UIAlertView alloc] initWithTitle:name message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//        }

        if (!ABRecordSetValue(aRecord, kABPersonLastNameProperty, (__bridge CFStringRef)[self.lastNames objectAtIndex:(arc4random() % self.lastNames.count)], &anError))
        {
            [[[UIAlertView alloc] initWithTitle:@"last name set fail." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            wantToSaveChanges = NO;
            break;
        }
//        else
//        {
//            NSString *name = (__bridge NSString *)ABRecordCopyValue(aRecord, kABPersonLastNameProperty);
//            [[[UIAlertView alloc] initWithTitle:name message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//        }

        ABMutableMultiValueRef multi = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueIdentifier multivalueIdentifier;

        NSString *sCellphone = [NSString stringWithFormat:@"015-%d%d%d%d-%d%d%d%d"
                                ,arc4random() % 10
                                ,arc4random() % 10
                                ,arc4random() % 10
                                ,arc4random() % 10
                                ,arc4random() % 10
                                ,arc4random() % 10
                                ,arc4random() % 10
                                ,arc4random() % 10];

        if (!ABMultiValueAddValueAndLabel(multi, (__bridge CFStringRef)sCellphone, kABPersonPhoneMainLabel, &multivalueIdentifier))
        {
            [[[UIAlertView alloc] initWithTitle:@"multi value set fail." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            wantToSaveChanges = NO;
            break;
        }
        else
        {
            if (!ABRecordSetValue(aRecord, kABPersonPhoneProperty, multi, &anError))
            {
                [[[UIAlertView alloc] initWithTitle:@"phone value set fail." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                wantToSaveChanges = NO;
                break;
            }
        }

        if (!ABAddressBookAddRecord(addressBook, aRecord, &anError))
        {
            [[[UIAlertView alloc] initWithTitle:@"add record fail." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            wantToSaveChanges = NO;
            break;
        }

        CFRelease(aRecord);
        CFRelease(multi);
    }

    if (ABAddressBookHasUnsavedChanges(addressBook))
    {
        if (wantToSaveChanges)
        {
            if (ABAddressBookSave(addressBook, &error))
            {
                [[[UIAlertView alloc] initWithTitle:@"저장완료" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"저장실패" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
        else
        {
            ABAddressBookRevert(addressBook);
        }
    }
    
    CFRelease(addressBook);
}

@end


@implementation RNContactManager (RNPrivate)

+ (NSArray *)firstNames
{
    NSError  *sError;
    NSString *sFilePath;
    NSString *sString;

    sError = nil;

    sFilePath = [[NSBundle mainBundle] pathForResource:@"firstnames" ofType:@"txt"];
    if (sFilePath)
    {
        sString = [NSString stringWithContentsOfFile:sFilePath encoding:NSUTF8StringEncoding error:&sError];
        if (sError)
        {
            [[[UIAlertView alloc] initWithTitle:[sError localizedDescription] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }

    return [sString componentsSeparatedByString:@"\n"];
}


+ (NSArray *)lastNames
{
    NSError  *sError;
    NSString *sFilePath;
    NSString *sString;

    sError = nil;

    sFilePath = [[NSBundle mainBundle] pathForResource:@"lastnames" ofType:@"txt"];
    if (sFilePath)
    {
        sString = [NSString stringWithContentsOfFile:sFilePath encoding:NSUTF8StringEncoding error:&sError];
        if (sError)
        {
            [[[UIAlertView alloc] initWithTitle:[sError localizedDescription] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }

    return [sString componentsSeparatedByString:@"\n"];
}

@end