//
//  RNViewController.m
//  ContactGenerator
//
//  Created by 권 오진 on 12. 7. 10..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "RNViewController.h"
#import "RNContactManager.h"


@interface RNViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UITextField *textField;
@end


@implementation RNViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setTitle:@"Contacts Generator"];

    UITableView *sTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [sTableView setAutoresizingMask:self.view.autoresizingMask];
    [sTableView setDataSource:self];
    [sTableView setDelegate:self];
    [self.view addSubview:sTableView];
    [self setTableView:sTableView];

    UITextView *sTextView = [[UITextView alloc] initWithFrame:CGRectMake(0.f, 0.f, CGRectGetWidth(self.view.frame), 200.f)];
    [sTextView setEditable:NO];
    [self setTextView:sTextView];
    [self.tableView setTableHeaderView:sTextView];
}


#pragma mark UITableViewDataSource, UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)aSection
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)aIndexPath
{
    UITableViewCell *sCell = [aTableView dequeueReusableCellWithIdentifier:NSStringFromClass(self.class)];
    if (sCell == nil)
    {
        sCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass(self.class)];
    }

    if ([aTableView rn_isLastIndexPath:aIndexPath])
    {
        [sCell.textLabel setText:@"Generate Persons"];
    }
    else
    {
        [sCell.textLabel setText:@""];
    }

    return sCell;
}


-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)aIndexPath
{
    [aTableView deselectRowAtIndexPath:aIndexPath animated:YES];

    if ([aTableView rn_isLastIndexPath:aIndexPath])
    {
        NSLog(@"Last cell selected.");
    }
}

@end
