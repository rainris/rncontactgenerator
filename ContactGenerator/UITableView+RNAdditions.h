//
//  UITableView+RNAdditions.h
//  ContactGenerator
//
//  Created by 레인 on 13. 4. 24..
//
//


#import <UIKit/UIKit.h>


@interface UITableView (RNAdditions)
- (BOOL)rn_isLastIndexPath:(NSIndexPath *)aIndexPath;
@end
