//
//  RNContactManager.h
//  ContactGenerator
//
//  Created by 레인 on 13. 4. 23..
//
//


#import <Foundation/Foundation.h>


@interface RNContactManager : NSObject
+ (void)generateForContactCount:(NSUInteger)aCount completion:(void(^)())aCompletionBlock;
@end
