//
//  RNAppDelegate.h
//  ContactGenerator
//
//  Created by 권 오진 on 12. 7. 10..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface RNAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@end
