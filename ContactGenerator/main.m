//
//  main.m
//  ContactGenerator
//
//  Created by 권 오진 on 12. 7. 10..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RNAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RNAppDelegate class]));
    }
}
