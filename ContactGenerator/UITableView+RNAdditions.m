//
//  UITableView+RNAdditions.m
//  ContactGenerator
//
//  Created by 레인 on 13. 4. 24..
//
//


#import "UITableView+RNAdditions.h"


@implementation UITableView (RNAdditions)

- (BOOL)rn_isLastIndexPath:(NSIndexPath *)aIndexPath
{
    return (aIndexPath.section == (self.numberOfSections - 1)
            && (aIndexPath.row == ([self numberOfRowsInSection:(self.numberOfSections - 1)] - 1)));
}

@end
